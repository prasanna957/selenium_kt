package com.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class locatingByID {
    WebDriver driver;

    @BeforeClass
    public void initWebDriver(){
        System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
        driver=new ChromeDriver();
    }



    @Test
    public void fbDemoSite(){
        //todo locate by tag  locate by css selector  have to cover
        driver.get("http://demo.guru99.com/test/facebook.html");
        WebElement emailField =driver.findElement(By.id("email"));
        emailField.sendKeys("prasanna957@gmail.com");
        System.out.println(emailField.getAttribute("value"));
    }



    @AfterClass
    public  void closeBrowser(){
    }


}
