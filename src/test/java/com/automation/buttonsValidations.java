package com.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class buttonsValidations {
    WebDriver driver;

    @BeforeClass
    public void initWebDriver(){
        System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
        driver=new ChromeDriver();
    }



    @Test
    public void openGoogle(){
        driver.get("https://www.google.co.in");
        WebElement textField =driver.findElement(By.xpath("/html/body/div[2]/div[2]/form/div[2]/div[1]/div[1]/div/div[2]/input"));
        textField.sendKeys("Hello");
        textField.sendKeys(Keys.ENTER);
        WebElement resultCounter= driver.findElement(By.xpath("/html/body/div[7]/div[2]/div[7]/div/div/div/div/div"));
        System.out.println(resultCounter.getText());
        assert resultCounter.getText().contains("About 2,76,00,00,000 results");
    }



    @AfterClass
    public  void closeBrowser(){
        driver.close();
    }


}
