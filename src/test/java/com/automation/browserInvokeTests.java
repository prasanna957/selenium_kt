package com.automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.logging.SocketHandler;


/**
 * Unit test for simple App.
 */
public class browserInvokeTests
{
    /**
     * Rigorous Test :-)
     */

    WebDriver driver;

    @BeforeClass
    public void initWebDriver(){
        System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
        driver=new ChromeDriver();
    }



    @Test
    public void openGoogle(){
        driver.get("https://www.google.com");
        Assert.assertEquals(driver.getTitle(),"Google");
    }

    @Test
    public void openYahoo(){
        driver.get("https://www.yahoo.com");
        Assert.assertEquals(driver.getTitle(),"Yahoo India | News, Finance, Cricket, Lifestyle and Entertainment");
    }

    @AfterClass
    public  void closeBrowser(){
        driver.close();
    }





}
